#ifndef __HISTOPROJECTION_H
#define __HISTOPROJECTION_H

#include "Histo1D.h"

class HistoProjection : public Histo1D
{
public:
    /// Constructor
    HistoProjection();
    /// Deconstructor
    ~HistoProjection();

private:
    void drawHisto();
    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);
};

#endif //__HISTOPROJECTION_H
