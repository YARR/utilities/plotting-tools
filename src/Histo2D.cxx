#include "Histo2D.h"

//////////////
/// Public ///
//////////////
//==============================
Histo2D::Histo2D() {
#ifdef DEBUG
    std::cout << "Histo2D::Histo2D()" << std::endl;
#endif
};
Histo2D::~Histo2D() {
#ifdef DEBUG
    std::cout << "Histo2D::~Histo2D()" << std::endl;
#endif
};
//==============================
/////////////////
/// Protected ///
/////////////////
//==============================
void Histo2D::setParameters(json& i_j) {
    PlotTool::setParameters(i_j);
}
void Histo2D::setParameters(TH1* i_h) {
    PlotTool::setParameters(i_h);
}
void Histo2D::setParameters(TH2* i_h) {
    PlotTool::setParameters(i_h);
}
//==============================
void Histo2D::writeHisto() {
    PlotTool::writeHisto();
}
void Histo2D::buildHisto() {
#ifdef DEBUG
    std::cout << "Histo2D::buildHisto()" << std::endl;
#endif
    PlotTool::buildHisto();

    int nbinsx             = m_axis.histo.x.nbins;
    float xlow             = m_axis.histo.x.low;
    float xhigh            = m_axis.histo.x.high;
    int nbinsy             = m_axis.histo.y.nbins;
    float ylow             = m_axis.histo.y.low;
    float yhigh            = m_axis.histo.y.high;
    m_h = new TH2F(m_histo_name.c_str(), "", nbinsx, xlow, xhigh, nbinsy, ylow, yhigh);

    std::string xaxistitle = m_axis.histo.x.title;
    std::string yaxistitle = m_axis.histo.y.title;
    std::string zaxistitle = m_axis.histo.z.title;
    style_TH2((TH2*)m_h, xaxistitle.c_str(), yaxistitle.c_str(), zaxistitle.c_str());

    if (m_axis.histo.x.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.x.label.size(); i++) m_h->GetXaxis()->SetBinLabel(i+1, m_axis.histo.x.label[i].c_str());
        m_h->GetXaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
    if (m_axis.histo.y.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.y.label.size(); i++) m_h->GetYaxis()->SetBinLabel(i+1, m_axis.histo.y.label[i].c_str());
        m_h->GetYaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
};
void Histo2D::setCanvas() {
#ifdef DEBUG
    std::cout << "Histo2D::setCanvas()" << std::endl;
#endif
    m_c = new TCanvas(Form("canv_%s",m_h->GetName()), "", 800, 600);
    style_TH2Canvas(m_c);
};
void Histo2D::drawHisto() {
#ifdef DEBUG
    std::cout << "Histo2D::drawHisto()" << std::endl;
#endif
    m_c->cd();
    style_TH2((TH2*)m_h);
    double min = m_axis.histo.z.low;
    if (m_axis.histo.z.title.find("Noise Occupancy")!=std::string::npos) {
    } else if (m_axis.histo.z.title.find("Threshold")!=std::string::npos ||
               m_axis.histo.z.title.find("Noise")!=std::string::npos) {
        min = m_h->GetMinimum(1);
    }
    double max = m_axis.histo.z.high;
    if (min==max) max=max+1;
    m_h->GetZaxis()->SetRangeUser(min, max);
    if (m_axis.overwrite) {
        m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
        m_h->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
        m_h->GetZaxis()->SetRangeUser(m_axis.histo.z.low, m_axis.histo.z.high);
    }
    m_h->Draw("colz");
    m_c->Modified();
    m_c->Update();
};
void Histo2D::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
    PlotTool::printHisto(i_dir, i_print, i_ext);
}
void Histo2D::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "Histo2D::fillHisto(j)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    int nbinsy = j["y"]["Bins"];
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = j["Data"][col][row];
            m_h->SetBinContent(col+1, row+1, tmp);
        }
    }
};
void Histo2D::fillHisto(TH1* h) {};
void Histo2D::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "Histo2D::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = h->GetBinContent(col+1, row+1);
            double par_x = h->ProjectionX()->GetBinCenter(col+1);
            double par_y = h->ProjectionY()->GetBinCenter(row+1);
            m_h->SetBinContent(m_h->FindBin(par_x, par_y), tmp);
        }
    }
};
