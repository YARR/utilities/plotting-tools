#include "HistoRms.h"

//////////////
/// Public ///
//////////////
//==============================
HistoRms::HistoRms() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoRms::HistoRms()" << std::endl;
#endif
};
HistoRms::~HistoRms() {
#ifdef DEBUG
    std::cout << "HistoRms::~HistoRms()" << std::endl;
#endif
};
//==============================
///////////////
/// Private ///
///////////////
//==============================
void HistoRms::setParametersX(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoRms::setParametersX(TH2)" << std::endl;
#endif
    m_axis.histo.x.nbins = 6;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 6;
    m_axis.histo.x.title = "Deviation from the Mean [RMS] ";
}
void HistoRms::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoRms::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    m_c->cd();

    const char *LabelName[6] = {"1","2","3","4","5",">5"};
    for (int i=1; i<=6; i++) m_h->GetXaxis()->SetBinLabel(i, LabelName[i-1]);
    m_h->GetXaxis()->LabelsOption("h");
    gStyle->SetPaintTextFormat(".0f");
    m_h->SetMarkerSize(1.8);
    m_h->SetMarkerColor(1);
    m_h->Draw("TEXT0 SAME");

    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.25));

    m_c->Modified();
    m_c->Update();
};
void HistoRms::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
#ifdef DEBUG
    std::cout << "HistoRms::printHisto(" << i_dir << ", " << i_print << ", " << i_ext << ")" << std::endl;
#endif
    PlotTool::setZero(m_zeros);
    Histo1D::printHisto(i_dir, i_print, i_ext);
}
void HistoRms::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "HistoRms::fillHisto(i_j)" << std::endl;
#endif
    double mean = m_dist->GetMean();
    double rms = m_dist->GetRMS();
    m_zeros = 0;
    int nbinsx = m_axis.data.x.nbins;
    int nbinsy = m_axis.data.y.nbins;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_j["Data"][col][row];
            int bin_rms = whichSigma(tmp, mean, rms, 1, 6);
            m_h->AddBinContent(bin_rms);
            if (tmp==0) m_zeros++;
        }
    }
};
void HistoRms::fillHisto(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoRms::fillHisto(TH2)" << std::endl;
#endif
    double mean = m_dist->GetMean();
    double rms = m_dist->GetRMS();
    m_zeros = 0;
    int nbinsx = i_h->GetNbinsX();
    int nbinsy = i_h->GetNbinsY();
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_h->GetBinContent(col+1, row+1);
            int bin_rms = whichSigma(tmp, mean, rms, 1, 6);
            m_h->AddBinContent(bin_rms);
            if (tmp==0) m_zeros++;
        }
    }
};
